import { db } from 'src/lib/db'
import type { QueryResolvers, MutationResolvers } from 'types/graphql'

export const pizzas: QueryResolvers['pizzas'] = () => {
  return db.pizza.findMany()
}

export const pizza: QueryResolvers['pizza'] = ({ id }) => {
  return db.pizza.findUnique({
    where: { id },
  })
}

export const createPizza: MutationResolvers['createPizza'] = ({ input }) => {
  return db.pizza.create({
    data: input,
  })
}

export const updatePizza: MutationResolvers['updatePizza'] = ({
  id,
  input,
}) => {
  return db.pizza.update({
    data: input,
    where: { id },
  })
}

export const deletePizza: MutationResolvers['deletePizza'] = ({ id }) => {
  return db.pizza.delete({
    where: { id },
  })
}
