import type { Prisma } from '@prisma/client'

export const standard = defineScenario<Prisma.PizzaCreateArgs>({
  pizza: {
    one: {
      data: { name: 'String', imageUrl: 'String', description: 'String' },
    },
    two: {
      data: { name: 'String', imageUrl: 'String', description: 'String' },
    },
  },
})

export type StandardScenario = typeof standard
