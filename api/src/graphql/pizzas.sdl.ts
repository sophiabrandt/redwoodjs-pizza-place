export const schema = gql`
  type Pizza {
    id: Int!
    name: String!
    imageUrl: String!
    description: String!
  }

  type Query {
    pizzas: [Pizza!]! @skipAuth
    pizza(id: Int!): Pizza @skipAuth
  }

  input CreatePizzaInput {
    name: String!
    imageUrl: String!
    description: String!
  }

  input UpdatePizzaInput {
    name: String
    imageUrl: String
    description: String
  }

  type Mutation {
    createPizza(input: CreatePizzaInput!): Pizza! @requireAuth
    updatePizza(id: Int!, input: UpdatePizzaInput!): Pizza! @requireAuth
    deletePizza(id: Int!): Pizza! @requireAuth
  }
`
