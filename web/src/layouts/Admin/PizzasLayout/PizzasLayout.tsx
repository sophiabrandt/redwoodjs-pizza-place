import { Link, routes } from '@redwoodjs/router'
import { Toaster } from '@redwoodjs/web/toast'

type PizzaLayoutProps = {
  children: React.ReactNode
}

const PizzasLayout = ({ children }: PizzaLayoutProps) => {
  return (
    <div className="rw-scaffold">
      <Toaster toastOptions={{ className: 'rw-toast', duration: 6000 }} />
      <header className="rw-header">
        <h1 className="rw-heading rw-heading-primary">
          <Link
            to={routes.adminPizzas()}
            className="rw-link"
          >
            Pizzas
          </Link>
        </h1>
        <Link
          to={routes.adminNewPizza()}
          className="rw-button rw-button-green"
        >
          <div className="rw-button-icon">+</div> New Pizza
        </Link>
      </header>
      <main className="rw-main">{children}</main>
    </div>
  )
}

export default PizzasLayout
