import humanize from 'humanize-string'

import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'
import { Link, routes, navigate } from '@redwoodjs/router'

const DELETE_PIZZA_MUTATION = gql`
  mutation DeletePizzaMutation($id: Int!) {
    deletePizza(id: $id) {
      id
    }
  }
`

const formatEnum = (values: string | string[] | null | undefined) => {
  if (values) {
    if (Array.isArray(values)) {
      const humanizedValues = values.map((value) => humanize(value))
      return humanizedValues.join(', ')
    } else {
      return humanize(values as string)
    }
  }
}

const jsonDisplay = (obj) => {
  return (
    <pre>
      <code>{JSON.stringify(obj, null, 2)}</code>
    </pre>
  )
}

const timeTag = (datetime) => {
  return (
    datetime && (
      <time dateTime={datetime} title={datetime}>
        {new Date(datetime).toUTCString()}
      </time>
    )
  )
}

const checkboxInputTag = (checked) => {
  return <input type="checkbox" checked={checked} disabled />
}

const Pizza = ({ pizza }) => {
  const [deletePizza] = useMutation(DELETE_PIZZA_MUTATION, {
    onCompleted: () => {
      toast.success('Pizza deleted')
      navigate(routes.adminPizzas())
    },
    onError: (error) => {
      toast.error(error.message)
    },
  })

  const onDeleteClick = (id) => {
    if (confirm('Are you sure you want to delete pizza ' + id + '?')) {
      deletePizza({ variables: { id } })
    }
  }

  return (
    <>
      <div className="rw-segment">
        <header className="rw-segment-header">
          <h2 className="rw-heading rw-heading-secondary">Pizza {pizza.id} Detail</h2>
        </header>
        <table className="rw-table">
          <tbody>
            <tr>
              <th>Id</th>
              <td>{pizza.id}</td>
            </tr><tr>
              <th>Name</th>
              <td>{pizza.name}</td>
            </tr><tr>
              <th>Image url</th>
              <td>{pizza.imageUrl}</td>
            </tr><tr>
              <th>Description</th>
              <td>{pizza.description}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <nav className="rw-button-group">
        <Link
          to={routes.adminEditPizza({ id: pizza.id })}
          className="rw-button rw-button-blue"
        >
          Edit
        </Link>
        <button
          type="button"
          className="rw-button rw-button-red"
          onClick={() => onDeleteClick(pizza.id)}
        >
          Delete
        </button>
      </nav>
    </>
  )
}

export default Pizza
