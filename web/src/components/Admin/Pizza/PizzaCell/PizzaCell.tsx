import type { FindPizzaById } from 'types/graphql'
import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'

import Pizza from 'src/components/Admin/Pizza/Pizza'

export const QUERY = gql`
  query FindPizzaById($id: Int!) {
    pizza: pizza(id: $id) {
      id
      name
      imageUrl
      description
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <div>Pizza not found</div>

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error.message}</div>
)

export const Success = ({ pizza }: CellSuccessProps<FindPizzaById>) => {
  return <Pizza pizza={pizza} />
}
