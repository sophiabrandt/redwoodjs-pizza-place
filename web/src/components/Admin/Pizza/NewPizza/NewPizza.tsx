import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'
import { navigate, routes } from '@redwoodjs/router'
import PizzaForm from 'src/components/Admin/Pizza/PizzaForm'

const CREATE_PIZZA_MUTATION = gql`
  mutation CreatePizzaMutation($input: CreatePizzaInput!) {
    createPizza(input: $input) {
      id
    }
  }
`

const NewPizza = () => {
  const [createPizza, { loading, error }] = useMutation(CREATE_PIZZA_MUTATION, {
    onCompleted: () => {
      toast.success('Pizza created')
      navigate(routes.adminPizzas())
    },
    onError: (error) => {
      toast.error(error.message)
    },
  })

  const onSave = (input) => {
    createPizza({ variables: { input } })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">New Pizza</h2>
      </header>
      <div className="rw-segment-main">
        <PizzaForm onSave={onSave} loading={loading} error={error} />
      </div>
    </div>
  )
}

export default NewPizza
