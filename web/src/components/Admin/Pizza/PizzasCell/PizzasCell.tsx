import type { FindPizzas } from 'types/graphql'
import type { CellSuccessProps, CellFailureProps } from '@redwoodjs/web'

import { Link, routes } from '@redwoodjs/router'

import Pizzas from 'src/components/Admin/Pizza/Pizzas'

export const QUERY = gql`
  query FindPizzas {
    pizzas {
      id
      name
      imageUrl
      description
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => {
  return (
    <div className="rw-text-center">
      {'No pizzas yet. '}
      <Link
        to={routes.adminNewPizza()}
        className="rw-link"
      >
        {'Create one?'}
      </Link>
    </div>
  )
}

export const Failure = ({ error }: CellFailureProps) => (
  <div className="rw-cell-error">{error.message}</div>
)

export const Success = ({ pizzas }: CellSuccessProps<FindPizzas>) => {
  return <Pizzas pizzas={pizzas} />
}
