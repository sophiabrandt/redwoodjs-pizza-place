import Pizza from './Pizza'
import { standard } from '../PizzasCell/PizzasCell.mock'

export const generated = () => {
  const { imageUrl, name } = standard().pizzas[0]
  return <Pizza imageUrl={imageUrl} name={name} />
}

export default { title: 'Components/Pizza' }
