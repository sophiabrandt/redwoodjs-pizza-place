const Pizza = ({ imageUrl, name }) => {
  return (
    <img
      src={imageUrl}
      alt={name}
    />
  );
};

export default Pizza;
