import { render } from '@redwoodjs/testing/web'

import Pizza from './Pizza'

//   Improve this test with help from the Redwood Testing Doc:
//    https://redwoodjs.com/docs/testing#testing-components

describe('Pizza', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<Pizza />)
    }).not.toThrow()
  })
})
