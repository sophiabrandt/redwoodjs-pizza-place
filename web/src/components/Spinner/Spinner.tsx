import SpinnerSvg from './spinner.svg'

const Spinner = () => {
  return (
    <div className="grid place-items-center min-h-screen">
      <SpinnerSvg />
    </div>
  )
}

export default Spinner
