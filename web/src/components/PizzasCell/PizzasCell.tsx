import type { CellFailureProps, CellSuccessProps } from "@redwoodjs/web";
import type { PizzasQuery } from "types/graphql";
import Pizza from "../Pizza/Pizza";
import Spinner from "../Spinner/Spinner";

export const QUERY = gql`
  query PizzasQuery {
    pizzas {
      id
      name
      imageUrl
      description
    }
  }
`

export const Loading = () => <Spinner />

export const Empty = () => <div>Empty</div>

export const Failure = ({ error }: CellFailureProps) => (
  <div style={{ color: 'red' }}>Error: {error.message}</div>
)

export const Success = ({ pizzas }: CellSuccessProps<PizzasQuery>) => {
  return (
    <ul className="carousel rounded-box">
      {pizzas.map((pizza) => {
        return (
          <li key={pizza.id} className="carousel-item">
            <Pizza name={pizza.name} imageUrl={pizza.imageUrl} />
          </li>
        )
      })}
    </ul>
  )
}
