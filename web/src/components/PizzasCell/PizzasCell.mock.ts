// Define your own mock data here:
export const standard = (/* vars, { ctx, req } */) => ({
  pizzas: [
    {
      id: 41,
      name: 'Pizza Pistacchio',
      imageUrl: 'https://api.lorem.space/image/pizza?w=400&h=300&hash=BDC01094',
      description:
        'A pizza with Pistachos and Zucchini, topped with Paneer and Cream Cheese.\n',
    },
    {
      id: 42,
      name: 'Alfred Papparoni',
      imageUrl: 'https://api.lorem.space/image/pizza?w=400&h=300&hash=7F5AE56A',
      description:
        'A pizza with Artichoke Hearts, Pickled Garlic, Butternut Squash, Hot Dogs, Tuna, Bacon, and Pulled Pork, topped with Mascarpone.\n',
    },
    {
      id: 23,
      name: 'Ginger Delight',
      imageUrl: 'https://api.lorem.space/image/pizza?w=400&h=300&hash=500B67FB',
      description:
        'A pizza with Tomatoes, Pickled Ginger, White Onion, Fennel, and Bannana Peppers, topped with White Sauce.\n',
    },
    {
      id: 81,
      name: 'Hotter than Hearts',
      imageUrl: 'https://api.lorem.space/image/pizza?w=400&h=300&hash=8B7BCDC2',
      description:
        'A pizza with Artichoke Hearts, Pickled Garlic, Butternut Squash, Hot Dogs, Tuna, Bacon, and Pulled Pork, topped with Mascarpone.\n',
    },
    {
      id: 94,
      name: 'Canadian Bacon',
      imageUrl: 'https://api.lorem.space/image/pizza?w=400&h=300&hash=A89D0DE6',
      description:
        'A pizza with Pickles, Pineapple, Pickled Jalapenos, Jalapenos, Pistachos, Bacon, Canadian Bacon, Ham, Meatballs, and Parsley.\n',
    },
  ],
})
