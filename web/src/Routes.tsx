// In this file, all Page components from 'src/pages` are auto-imported. Nested
// directories are supported, and should be uppercase. Each subdirectory will be
// prepended onto the component name.
//
// Examples:
//
// 'src/pages/HomePage/HomePage.js'         -> HomePage
// 'src/pages/Admin/BooksPage/BooksPage.js' -> AdminBooksPage

import { Set, Router, Route } from '@redwoodjs/router'
import PizzasLayout from 'src/layouts/Admin/PizzasLayout'

const Routes = () => {
  return (
    <Router>
      <Set wrap={PizzasLayout}>
        <Route path="/admin/pizzas/new" page={AdminPizzaNewPizzaPage} name="adminNewPizza" />
        <Route path="/admin/pizzas/{id:Int}/edit" page={AdminPizzaEditPizzaPage} name="adminEditPizza" />
        <Route path="/admin/pizzas/{id:Int}" page={AdminPizzaPizzaPage} name="adminPizza" />
        <Route path="/admin/pizzas" page={AdminPizzaPizzasPage} name="adminPizzas" />
      </Set>
      <Route path="/pizzas" page={PizzasPage} name="pizzas" />
      <Route notfound page={NotFoundPage} />
    </Router>
  )
}

export default Routes
