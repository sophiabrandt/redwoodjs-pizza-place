import { MetaTags } from "@redwoodjs/web";
import PizzasCell from "../../components/PizzasCell";

const PizzasPage = () => {
  return (
    <>
      <MetaTags title="Pizzas" description="Pizzas page" />
      <PizzasCell />
    </>
  )
}

export default PizzasPage
