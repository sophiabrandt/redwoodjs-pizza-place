import { render } from '@redwoodjs/testing/web'

import PizzasPage from './PizzasPage'

//   Improve this test with help from the Redwood Testing Doc:
//   https://redwoodjs.com/docs/testing#testing-pages-layouts

describe('PizzasPage', () => {
  it('renders successfully', () => {
    expect(() => {
      render(<PizzasPage />)
    }).not.toThrow()
  })
})
